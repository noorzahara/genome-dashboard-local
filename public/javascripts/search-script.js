let onPageSearch = () => {
    // Declare variables
    let header, i, txtValue;
    let input = document.getElementById('search_input');
    let filter = input.value.toUpperCase();
    let searchValues = document.getElementsByClassName('col');

    // Search for Header in each Element
    [ ...searchValues].forEach(element => {
        header = element.getElementsByTagName("h1")[0];
        txtValue = header.textContent || header.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
};

let clearSearch = () => {
    document.getElementById('search_input').value = '';
    onPageSearch();
};
