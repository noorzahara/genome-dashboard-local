let IGBURL = 'http://127.0.0.1:7085/igbStatusCheck';
let getIgbStatus;

fetch(IGBURL).then(function(response) {
    console.log({response})
    getIgbStatus = true
}).catch(function(err) {
    console.log('Fetch problem: ' + err.message);
    $('.toast').toast('show');
    getIgbStatus = false
});

let igbRedirect = (data) => {
    if(!getIgbStatus){
        $('#myModal').modal('toggle');
    } else {
        window.location = 'http://localhost:7085/IGBControl?version=' + data;
    }
};
